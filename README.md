# Dicom Viewer

## What is Dicom Viewer

Is a Programm which can load a Dicom Image and show the MetaData and manipulate it a little bit.

## Requirements

- Java Version 11
- JVM with JavaFx bundled 
- Maven installed locally or provided by an IDE
- Support for JavaFx on your platform

## Installation

### Via CLI

Run in the Project  Root
```mvn compile exec:java```
to install the dependcies and Run the Project

To Search for new Versions of Dependencies run in the Project Root
```mvn versions:display-dependency-updates```

### Via Intellij

Mark src as Source Root via Right Click on the Folder and Select afterwards src/at.zanko/GUI/Main as start Class 

## History

Dicom Viewer is a project which I made at [HTL Spengergasse](https://spengergasse.at) in the third grade in department biomedical informatics.

