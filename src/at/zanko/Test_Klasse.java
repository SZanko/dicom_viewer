package at.zanko;

import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Test_Klasse {
    public static void main(String[] args) throws IOException {



        // lese DICOM Datei ein
        // getShorts[] ->  pixls
        // lookupTable -> argb-Wert

        WritableImage img1 = new WritableImage(500,500);
        PixelWriter writer = img1.getPixelWriter();
        writer.setArgb(0,0,0);

        /*Double d = 7.99; // 7
        int i = d.intValue();

        System.out.printf("Double : %f, int : %d %n", d, i);
        int value = (int) 6.14; // 6
        int score = (int) 6.99; // 6

        System.out.printf("double : %f, int : %d %n", 6.14, value);
        System.out.printf("double : %f, int : %d %n", 6.99, score);

        int a = (int) Math.round(6.14); // 3
        int b = (int) Math.round(6.99); // 4
        int c = (int) Math.round(6.5); // 4

        System.out.printf("double : %f, int : %d %n", 3.14, a);
        System.out.printf("double : %f, int : %d %n", 3.99, b);
        System.out.printf("double : %f, int : %d %n", 3.5, c);
        */
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter String");
        String s = br.readLine();
        System.out.print("Enter Integer:");
        try{
            int i = Integer.parseInt(br.readLine());
        }catch(NumberFormatException nfe){
            System.err.println("Invalid Format!");
        }

    }
}
