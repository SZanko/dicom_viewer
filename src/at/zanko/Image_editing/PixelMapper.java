package at.zanko.Image_editing;

import at.zanko.Database.entities.I_instance;
import at.zanko.Interface.*;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class PixelMapper implements IPixelMapper {
    //Gib die Anzahl der Pixel aus, den Maximal Wert, den minimal Wert, durchschnitt Wert
    private int max=0;
    private int min=0;
    private Set<IObserver> observers=new HashSet<>();
    private IImagePanel imagePanel;
    private ILookupTable lookup_table;

    private short[] pixelDatenshort(File bearbeiteterFile) throws IOException {
        DicomInputStream input=new DicomInputStream(bearbeiteterFile);
        DicomObject dcm=input.readDicomObject();
        return dcm.getShorts(Tag.PixelData);
    }
    private int imagerows(File bearbeiteteterFile) throws IOException {
        DicomInputStream input=new DicomInputStream(bearbeiteteterFile);
        DicomObject dcm=input.readDicomObject();
        return dcm.getInt(Tag.Rows);

    }

    private int imagecolums(File bearbeiteteterFile) throws IOException {
        DicomInputStream input=new DicomInputStream(bearbeiteteterFile);
        DicomObject dcm=input.readDicomObject();
        return dcm.getInt(Tag.Columns);
    }

    public void Ausgeben_Eckdaten(File bearbeiteterFile) throws IOException
    {
        short [] pixelData=pixelDatenshort(bearbeiteterFile);
        int summe=0;
        for (int p:pixelData)
        {
            summe += p;
            if(p>max)
            {
                max=p;
            }
            if(p<min)
            {
                min=p;
            }

        }
        int mw = summe / pixelData.length;

        System.out.println("Max Pixel Wert: "+max);
        System.out.println("Min Pixel Wert: "+min);
        System.out.println("Summe aller Pixel: "+summe);
        System.out.println("Mittelwert der Pixel: "+ mw);
    }
    public WritableImage Image_formation(File bearbeitungsfile, int alpha, int center, int width) throws IOException,ArithmeticException {

        //DicomInputStream input=new DicomInputStream(bearbeitungsfile);
        short[] pixelData=pixelDatenshort(bearbeitungsfile);
        int rows = imagerows(bearbeitungsfile);
        int colums=imagecolums(bearbeitungsfile);
        WritableImage wi=new WritableImage(colums,rows);
        PixelWriter writer = wi.getPixelWriter();
        Lookup_Table lt=new Lookup_Table();
        lt.setAlpha(alpha);
        lt.setCenter(center);
        lt.setWidth(width);

        for (int r=0;r<rows;r++)
        {
            for(int c=0;c<colums;c++)
            {
                int value=lt.argb(pixelData[r*colums+c]);
                writer.setArgb(c,r,value);
            }
        }

        return wi;//ImageIO.write(bi,"png",new File("Export_image_"+lt.getWidth()+"_"+lt.getCenter()+"_"+lt.getAlpha())); Soll doch im Main geschehen
    }

    @Override
    public WritableImage map(I_instance instance) {
        String pfad=instance.getDicomFile();
        File bearbeitungsfile=new File(pfad);
        short[] pixelData= new short[0];
        try {
            pixelData = pixelDatenshort(bearbeitungsfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int rows = 0;
        try {
            rows = imagerows(bearbeitungsfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int colums= 0;
        try {
            colums = imagecolums(bearbeitungsfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        WritableImage wi=new WritableImage(colums,rows);
        PixelWriter writer = wi.getPixelWriter();
        Lookup_Table lt=new Lookup_Table();
        lt.setAlpha(50);
        lt.setCenter(1200);
        lt.setWidth(800);

        for (int r=0;r<rows;r++)
        {
            for(int c=0;c<colums;c++)
            {
                int value=lt.argb(pixelData[r*colums+c]);
                writer.setArgb(c,r,value);
            }
        }

        return wi;
    }

    @Override
    public void configure(InterfaceBuilder builder) {
        this.lookup_table=builder.getLookupTable();
    }

    @Override
    public void registerObserver(IObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(IObserver o) {
        observers.remove(o);
    }

    @Override
    public void changed(IObservable o) {
        this.imagePanel =(IImagePanel)o;
    }
}
