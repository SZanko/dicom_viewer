package at.zanko.Image_editing;

import at.zanko.Interface.*;

import java.util.HashSet;
import java.util.Set;

public class Lookup_Table implements ILookupTable {
    private int width;
    private int center;
    private int alpha;
    private Set<IObserver> observers=new HashSet<>();
    private IControlPanel controlPanel;
    private IPixelMapper pixelmapper;

    //Properties
    public int getCenter() {
        return center;
    }
    void setCenter(int center)
    {
        this.center = center;
    }
    public  int getWidth() {
        return width;
    }
    void setWidth(int width) {
        if(width!=0)
        {
            this.width = width;
        }
        else
        {
            throw new IllegalArgumentException("Width must not be 0");
        }
    }
    public  int getAlpha() {
        return alpha;
    }
    void setAlpha(int alpha) {
        if(alpha >= 0&& alpha<=100)
        {
            this.alpha = alpha;
        }
        else throw new IllegalArgumentException("alpha must be 0 to 100");
    }

    @Override
    public int argb(int value) {
        int result=(value-center+width/2)*255/width;
        if(result<0) result=0;
        if(result>255)result=255;

        return (alpha*255/100 << 24| result << 16|result << 8|result);
    }

    @Override
    public void configure(InterfaceBuilder builder) {
        controlPanel=builder.getControlPanel();
    }

    @Override
    public void registerObserver(IObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(IObserver o) {
        observers.remove(o);
    }

    @Override
    public void changed(IObservable o) {
        this.pixelmapper=(PixelMapper)o;
    }
}
