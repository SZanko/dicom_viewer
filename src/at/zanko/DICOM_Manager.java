package at.zanko;

import at.zanko.Database.entities.I_instance;
import at.zanko.Database.entities.Patient;
import at.zanko.Database.entities.Series;
import at.zanko.Database.entities.Study;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;

public class DICOM_Manager {

    private ArrayList<Patient> patients = new ArrayList<>();

    public  Patient Patientenbrowser(File datei) throws IOException {
        DicomInputStream input = new DicomInputStream(datei);
        DicomObject dcm=input.readDicomObject();

        String pID = (dcm.getString(Tag.PatientID));
        String pName = (dcm.getString(Tag.PatientName));
        java.util.Date pBD = dcm.getDate(Tag.PatientBirthDate);
        String StudyID = dcm.getString(Tag.StudyID);
        System.out.println("StudieID ist: "+StudyID);
        Date StudyDate = dcm.getDate(Tag.StudyDate);
        String StudyDescription = dcm.getString(Tag.StudyDescription);
        String SeriesID = dcm.getString(Tag.SeriesInstanceUID);
        System.out.println("SeriesID ist: "+SeriesID);
        String SeriesDescription = dcm.getString(Tag.SeriesDescription);
        String ImageID = dcm.getString(Tag.SOPInstanceUID);
        System.out.println("ImageID ist: "+ImageID);
        int ImageNumber = dcm.getInt(Tag.ReferencedReferenceImageNumber);
        int Rows = dcm.getInt(Tag.Rows);
        Integer Colums;
        Colums = dcm.getInt(Tag.Columns);


        I_instance instancet = new I_instance(ImageID, ImageNumber, Rows, Colums);    //tmp save Image from file
        System.out.println(instancet.toString());
        Series seriest = new Series(SeriesID, SeriesDescription);       //tmp save Series from file
        System.out.println(seriest.toString());
        Study studyt = new Study(StudyID, StudyDate, StudyDescription); //tmp save Study from file
        System.out.println(studyt.toString());
        Patient p_test = new Patient(pID, pName, pBD);                   //tmp save Patient from file


        seriest.add(instancet);
        studyt.add(seriest);
        p_test.add(studyt);
        /*
        if(!patients.contains(p)){                                 //if patient doesn´t exist yet -> Save
            patients.add(p);
        }
        for (Patient pat : patients){                              //To check data for redondance in a patient
            if(pat.getPatientID().equals(p.getPatientID()))
            {       //To check if data belongs to patient
                if (pat.getStudyList().contains(Sd))
                {
                    pat.getStudyList().add(Sd);
                }
                if (!pat.getSeriesList().contains(Ser))
                {
                    pat.getSeriesList().add(Ser);
                }
                if (!pat.getInstanceList().contains(I))
                {
                    pat.getInstanceList().add(I);
                }
            }
        }
        */
        return p_test;
    }


   /* public ArrayList<Patient> PatientList() throws Exception {
        File dicomFolder = new File(path);

        for (File f : dicomFolder.listFiles()) {
            DicomInputStream input = new DicomInputStream(new File(path));
            System.out.println(input);
            DicomObject dcm = input.readDicomObject();
            String pID = (dcm.getString(Tag.PatientID));
            String pName = (dcm.getString(Tag.PatientName));
            java.util.Date pBD = dcm.getDate(Tag.PatientBirthDate);
            String StudyID = dcm.getString(Tag.StudyID);
            Date StudyDate = dcm.getDate(Tag.StudyDate);
            String StudyDescription = dcm.getString(Tag.StudyDescription);
            String SeriesID = dcm.getString(Tag.SeriesInstanceUID);
            String SeriesDescription = dcm.getString(Tag.SeriesDescription);
            String ImageID = dcm.getString(Tag.SOPInstanceUID);
            Integer ImageNumber = dcm.getInt(Tag.ReferencedReferenceImageNumber);
            Integer Rows = dcm.getInt(Tag.Rows);
            Integer Colums = dcm.getInt(Tag.Columns);

            Instance instancet = new Instance(ImageID, ImageNumber, Rows, Colums);    //tmp save Image from file
            Series seriest = new Series(SeriesID, SeriesDescription);       //tmp save Series from file
            Study studyt = new Study(StudyID, StudyDate, StudyDescription); //tmp save Study from file
            Patient patientT = new Patient(pID, pName, pBD);                   //tmp save Patient from file

            if(!patients.contains(patientT)){                                 //if patient doesn´t exist yet -> Save
                patients.add(p);
            }
            for (Patient pat : patients){                              //To check data for redondance in a patient
                if(pat.getPatientID().equals(p.getPatientID()))
                {       //To check if data belongs to patient
                    if (pat.getStudyList().contains(studyt))
                    {
                        pat.getStudyList().add(studyt);
                    }
                    if (!pat.getSeriesList().contains(seriest))
                    {
                        pat.getSeriesList().add(seriest);
                    }
                    if (!pat.getInstanceList().contains(instancet))
                    {
                        pat.getInstanceList().add(instancet);
                    }
                }
            }
        }
        return patients;
    }*/
}
