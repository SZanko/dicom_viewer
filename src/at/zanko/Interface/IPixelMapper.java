package at.zanko.Interface;

import at.zanko.Database.entities.I_instance;
import javafx.scene.image.WritableImage;

/**
 * a pixel mapper calculates a Writeable image from the pixel data
 * contained in the DICOM file referenced by the given Instance object
 */
public interface IPixelMapper extends  IModuleBase {
    /**
     * maps (calculates) the image from the pixel data of instance
     * @param instance reference to instance from which to calculate BufferedImage
     * @return new WriteableImage
     * @throws Exception if pixel data cannot be read from DICOM foöe
     */
    WritableImage map(I_instance instance) throws Exception;
}
