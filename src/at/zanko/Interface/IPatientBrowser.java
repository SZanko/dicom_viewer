package at.zanko.Interface;

import at.zanko.Database.entities.I_instance;
import at.zanko.Database.entities.Patient;
import at.zanko.Database.entities.Series;
import at.zanko.Database.entities.Study;

/**
 * a patient browser shows all the patients, studies, ...
 * available in the patient repository.
 * it must actively retrieve this information from the patient repository
 */
public interface IPatientBrowser extends IModuleBase, IModuleUI {
    /**
     * return patient which is currently selected
     * or which is owner of the currently selected object.
     * @return selected patient or null if none
     */
    Patient selectedPatient();

    /**
     * return study which is currently selected
     * or which is owner of the currently selected object;
     * (returns null if selected object is a patient)
     * @return selected study or null if none
     */
    Study selectedStudy();

    /**
     * return series which is currently selected
     * or which is owner of the currently selected Instance;
     * (returns null if selected object is a patient or a study)
     * @return selected series or null if none
     */
    Series selectedSeries();

    /**
     * return Instance which is currently selected
     * (returns null if selected object is a patient, study or series)
     * @return selected series or null if none
     */
    I_instance selectedInstance();
}
