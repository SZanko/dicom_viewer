package at.zanko.GUI;

import at.zanko.Database.entities.I_instance;
import at.zanko.Database.entities.Patient;
import at.zanko.Database.entities.Series;
import at.zanko.Database.entities.Study;
import at.zanko.Interface.IObservable;
import at.zanko.Interface.IObserver;
import at.zanko.Interface.IPatientBrowser;
import at.zanko.Interface.InterfaceBuilder;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

import java.util.HashSet;
import java.util.Set;

import static at.zanko.GUI.Layout.baum;

public class PatientBrowser extends Baum implements IPatientBrowser {

    private Set<IObserver> observers = new HashSet<>();

    @Override
    public Patient selectedPatient() {
        Patient p=new Patient();

        return p;
    }

    @Override
    public Study selectedStudy() {
        return null;
    }

    @Override
    public Series selectedSeries() {
        return null;
    }

    @Override
    public I_instance selectedInstance() {
        return null;
    }

    @Override
    public void configure(InterfaceBuilder builder) {

    }

    @Override
    public Node uiComponent() {
        return null;
    }

    @Override
    public void registerObserver(IObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(IObserver o) {
        observers.remove(o);
    }

    public void notifyObservers() {
        for(IObserver o : observers) {
            o.changed(this);
        }
    }

    @Override
    public void changed(IObservable o) {
        ImagePanel imagePanel = (ImagePanel)o;
    }
    //Soll die einzelnen DICOM Objekte hierarchisch ausgeben
    StackPane browserwindow() {

        StackPane obensp = new StackPane();
        GridPane obeng = new GridPane();
        ScrollPane scrollPane = new ScrollPane();
        baum.setPrefSize(400, 200);
        scrollPane.setPrefSize(400, 200);
        scrollPane.setContent(baum);//Setzt jetzt eine leere TreeView in welche ich später was einfüge
        Label bereichname = new Label("Patients");
        obensp.setPrefSize(400, 200);
        //Legt die Objekte im Gridview fest
        obeng.add(bereichname, 0, 0);
        obeng.add(scrollPane, 0, 1);
        obensp.getChildren().addAll(obeng);
        return obensp;
    }
}
