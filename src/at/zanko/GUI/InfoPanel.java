package at.zanko.GUI;

import at.zanko.Interface.*;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

import java.util.HashSet;
import java.util.Set;

public class InfoPanel implements IInfoPanel {
    private Label info;
    private IInfoPanel infopanel;
    private IPatientBrowser patientBrowser;
    // usw

    static Label text;
    //Soll die Info zum Bild anzeigen
    //Muss noch geschrieben werden jetzt wird mal nur ein Test Text ausgegeben
    private Set<IObserver> observers=new HashSet<>();

    StackPane informationdisp() {
        StackPane mitteresPane = new StackPane();
        GridPane mitteg = new GridPane();
        Label bereichname = new Label("Info");
        this.info = new Label("Info_Test");
        mitteresPane.setPrefSize(400, 200);


        mitteg.add(bereichname, 0, 1, 1, 2);

        mitteg.add(this.info, 1, 4);

        mitteresPane.getChildren().addAll(mitteg);//Hügt das Label dem Stackpane hinzu
        return mitteresPane;
    }

    void changeLabelText()
    {

    }

    @Override
    public void configure(InterfaceBuilder builder) {
        this.patientBrowser = builder.getPatientBrowser();
        this.infopanel = builder.getInfoPanel();
    }

    @Override
    public Node uiComponent() {

        return null;
    }

    @Override
    public void registerObserver(IObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(IObserver o) {
        observers.remove(o);
    }

    @Override
    public void changed(IObservable o) {
        this.patientBrowser = (PatientBrowser)o;

        //patientBrowser.selectedPatient()
    }
}
