package at.zanko.GUI;

import at.zanko.Interface.IImagePanel;
import at.zanko.Interface.IObservable;
import at.zanko.Interface.IObserver;
import at.zanko.Interface.InterfaceBuilder;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

import java.util.HashSet;
import java.util.Set;

import static at.zanko.GUI.Layout.bildview;

public class ImagePanel implements IImagePanel {

    //Rechtes Panel soll ein Bild dann anzeigen wenn eines geöffnet wurde und wenn kein Bild geöffnet ist soll es einen Text rausgeben das nichts geöffnet ist
    StackPane createImagePanel()
    {
        StackPane st = new StackPane();
        Label feldbeschriftung = new Label("Images");
        StackPane.setAlignment(feldbeschriftung, Pos.TOP_LEFT);
        st.getChildren().add(feldbeschriftung);

        bildview = new ImageView();
        bildview.setPreserveRatio(false);
        bildview.fitHeightProperty().bind(st.heightProperty());//Legt die maximal Höhe für das Bild fest
        bildview.fitWidthProperty().bind(st.widthProperty());//Legt die maximal Länge für das Bild fest
        //Label text=new Label("Kein File geöffnet");
        st.getChildren().add(bildview);
        return st;
    }





    private Set<IObserver> observers=new HashSet<>();
    @Override
    public void configure(InterfaceBuilder builder) {

    }

    @Override
    public Node uiComponent() {
        return null;
    }

    @Override
    public void registerObserver(IObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(IObserver o) {
        observers.remove(o);
    }

    @Override
    public void changed(IObservable o) {

    }
}
