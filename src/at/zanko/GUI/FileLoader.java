package at.zanko.GUI;

import at.zanko.DICOM_Manager;
import at.zanko.Database.entities.I_instance;
import at.zanko.Database.entities.Patient;
import at.zanko.Image_editing.PixelMapper;
import at.zanko.Interface.IFileLoader;
import at.zanko.Interface.IObservable;
import at.zanko.Interface.IObserver;
import at.zanko.Interface.InterfaceBuilder;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.io.DicomInputStream;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.Set;

class FileLoader implements IFileLoader {
    private Window filestage;
    String dateipfad;
    MenuItem speichern;

    public Window getFilestage() {
        return filestage;
    }

    public void setFilestage(Window filestage) {
        this.filestage = filestage;
    }

    private Image bild;//Das Bild was später in die Image View kommt
    private Set<IObserver> observers=new HashSet<>();

    void Fileoeffnen() throws MalformedURLException {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Auswählen der Dicom Datei");

        chooser.getExtensionFilters().addAll(//
                new FileChooser.ExtensionFilter("DICOM Files", "*.dcm"), //Dateiendung braucht umbedingt einen Stern vor dem Punkt ähnlich wie in SQL
                new FileChooser.ExtensionFilter("DICOM Files in JPG", "*.jpg"), //
                new FileChooser.ExtensionFilter("DICOM Files in PNG", "*.png"));


        File datei = chooser.showOpenDialog(this.filestage);
        if(datei!=null) {
            System.out.println(datei.getAbsolutePath());//der Pfad zur Datei
            this.dateipfad = datei.toURI().toURL().toString();
            String dateiname=datei.getName();
            System.out.println(dateiname);
            int anzahlbuchstaben=dateiname.length();
            System.out.println("Der Dateiname besteht aus mindestens "+anzahlbuchstaben+" Buchstaben");
            String dateiendung=dateiname.substring(anzahlbuchstaben-4);//Zum bekommen der Dateiendung
            System.out.println(dateiendung);
            String dateiordner=dateipfad.substring(0,dateipfad.length()-anzahlbuchstaben);
            System.out.println("Der Ordner der Datei ist: "+dateiordner);
            Main.Status.setText("Status: " + dateipfad + " geöffnet");
            System.out.println(dateiendung+" Datei wird geöffnet");

            if(dateiendung.equals(".dcm"))//Nachschauen ob es eine DICOM Datei ist
            {
                DICOM_Manager dcm=new DICOM_Manager();
                try
                {
                    Patient test_p=dcm.Patientenbrowser(datei);
                    System.out.println(test_p.toString());
                    Baum baumitem=new Baum();
                    Layout.baum.setRoot(baumitem.add(test_p));
                    Layout.baum.getSelectionModel().selectedItemProperty().addListener((v,oldValue,newValue)->{

                        if(newValue!=null)
                        {
                            System.out.println(newValue);
                        }
                        Layout.baum.setEditable(true);
                    });
                    PixelMapper im_e=new PixelMapper();
                    DicomInputStream input=new DicomInputStream(datei);
                    DicomObject dco=input.readDicomObject();
                    I_instance instance=new I_instance(dco,datei);
                    //bild=im_e.Image_formation(datei,50,800,800);

                    bild= im_e.map(instance);
                    ControllPanel.setCdatei(datei);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
            else
            {
                System.out.println("im else");
                bild =new Image(dateipfad);
            }
            Layout.bildview.setImage(bild);
            speichern.setDisable(false);
        }
        else
        {
            Alert alert=new Alert(Alert.AlertType.INFORMATION);//Falls kein File Ausgewählt wurde
            alert.setTitle("Infomartions Dialog");
            alert.setHeaderText("Kein File ausgewählt");
            alert.showAndWait();
        }
    }

    @Override
    public void loadFolder(File folder) {

    }

    @Override
    public void configure(InterfaceBuilder builder) {

    }

    @Override
    public void registerObserver(IObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(IObserver o) {
        observers.remove(o);
    }

    @Override
    public void changed(IObservable o) {

    }
}
