package at.zanko.GUI;

import javafx.event.ActionEvent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.net.MalformedURLException;


class Menu_setup extends FileLoader {

    private static Window filestage;



    //Erschafft die Menuleiste mit Menüpunkten welche einen File Browser öffnen wo man einen File auswählen kann
    //Funktioniert bis jetzt ruft alle Teile auf die bis jetzt inizialiesiert wurden
    MenuBar menuErschaffen()
    {
        Menu filemenu=new Menu("_File");
        this.speichern=new MenuItem();

        MenuItem oeffnen= new MenuItem("öffnen");
        oeffnen.setOnAction(actionEvent ->
        {
            try {
                Fileoeffnen();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        });//legt fest das wenn draufgeklickt wird die Methode aufgerufen wird

        speichern=new MenuItem("speichern");
        speichern.setOnAction(this::Filespeichern);
        speichern.setDisable(true);//Damit die Option Speichern nicht aufgerufen werden kann bis ein File geöffnet ist

        SeparatorMenuItem smi= new SeparatorMenuItem();

        filemenu.getItems().add(oeffnen);
        filemenu.getItems().add(new MenuItem("zuletzt geöffnet"));
        filemenu.getItems().add(smi);
        filemenu.getItems().add(speichern);
        MenuBar menuBar=new MenuBar();
        menuBar.getMenus().add(filemenu);
        return menuBar;

    }

    //Wird gestartet wenn man beim Menu den Button speichern auswählt öffnet einen Speicherdialog wo man den File Speichern kann
    //Noch nicht getestet
    private  void  Filespeichern(ActionEvent actionEvent) {

        FileChooser chooser=new FileChooser();
        chooser.setTitle("File speichern unter");
        chooser.showSaveDialog(filestage );
        //setzenvonStatus("Datei gespeichert"); alt kann gelöscht werden da die Methode nicht mehr existiert
        Main.Status.setText("Status: "+dateipfad+"gespeichert");
    }

    //Öffnet einen File OpenDialog und setzt die Speicher Option im Menu auf aktiv
    //Getested Updated den Status wie erhofft und man kann auch zwischen verschiedenen File Typen auswählen
    //Kann alle Files öffnen jedoch werden noch keine dcm Files dann in der ImageView angezeigt

}