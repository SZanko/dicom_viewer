package at.zanko.GUI;

import at.zanko.Image_editing.PixelMapper;
import at.zanko.Interface.IControlPanel;
import at.zanko.Interface.IObservable;
import at.zanko.Interface.IObserver;
import at.zanko.Interface.InterfaceBuilder;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static at.zanko.GUI.Layout.bildview;

public class ControllPanel extends FileLoader implements IControlPanel {
    private static File cdatei;
    private int alphawert = 50;
    private int widthwert = 1200;
    private int centerwert = 800;
    private Set<IObserver> observers = new HashSet<>();


    static void setCdatei(File csdatei) {
        cdatei = csdatei;
    }

    @Override
    public int getCenter() {
        return centerwert;
    }

    @Override
    public int getWidth() {
        return widthwert;
    }

    @Override
    public int getAlpha() {
        return alphawert;
    }

    @Override
    public void configure(InterfaceBuilder builder) {

    }

    @Override
    public Node uiComponent() {
        return null;
    }

    @Override
    public void registerObserver(IObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(IObserver o) {
        observers.remove(o);
    }

    @Override
    public void changed(IObservable o) {

    }
    private int convertSliderValues(Number newValue)
    {
        double zwl = (double) newValue;
        return (int)zwl;
    }
    //Soll das unterste von den 3 Stackpanes sein mit 3 Slidern wobei jeder Slider mit einen Label beschriftet ist
    //Funktioniert bis auf das Slider noch keine Methoden haben
    StackPane slider_pane() {
        Label bereichname = new Label("Contrast");//Anzeige Name des Stackpanels
        GridPane formatierung = new GridPane();
        formatierung.add(bereichname, 0, 0);//Setzt den Anzeigenamen des Stackpanels im Userinterface

        StackPane unten = new StackPane();

        unten.setPrefSize(400, 200);
        Label widthl = new Label("Width");
        Label centerl = new Label("Center");
        Label alphal = new Label("Alpha");
        Slider width = new Slider(50, 1500, 800);
        Slider center = new Slider(500, 1500, 1200);
        Slider alpha = new Slider(1, 100, 50);//Slider für die Alpha Werte des Bildes
        unten.setStyle("-fx-background-color: #55ffff");
        PixelMapper image_e=new PixelMapper();

        //Events für die Slider
        alpha.valueProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("Alpha von " + oldValue + " auf " + newValue + " gestellt");
            try {
                Image testbild =image_e.Image_formation(getDatei(), convertSliderValues(newValue),  getCenter(),getWidth());
                bildview.setImage(testbild);

                 alphawert= convertSliderValues(newValue);
            } catch (IOException e) {
                System.out.println("Abgestürzt");
                e.printStackTrace();
            }
        });
        center.valueProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("Center von " + oldValue + " auf " + newValue + " gestellt");
            try {
                Image testbild = image_e.Image_formation(getDatei(),getAlpha(), convertSliderValues(newValue), getWidth());
                bildview.setImage(testbild);
                //New Value muss zuerst zu einen Double zugewissen werden statt Int weil es sonst eine Nullpointer Exeption gibt
                centerwert = convertSliderValues(newValue);
            } catch (IOException e) {
                System.out.println("Abgestürzt");
                e.printStackTrace();
            }

        });
        width.valueProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("Width von " + oldValue + " auf " + newValue + " gestellt");
            try {
                WritableImage testbild = image_e.Image_formation(getDatei(), getAlpha(), getWidth(), convertSliderValues(newValue));
                bildview.setImage(testbild);

                widthwert = convertSliderValues(newValue);
            } catch (IOException e) {
                System.out.println("Abgestürzt");
                e.printStackTrace();
            }
        });

        //Hinzufügen der Elemente in die Gridpane
        formatierung.add(alphal, 1, 4);
        formatierung.add(alpha, 2, 4);
        formatierung.add(centerl, 1, 3);
        formatierung.add(center, 2, 3);
        formatierung.add(widthl, 1, 2);
        formatierung.add(width, 2, 2);
        unten.getChildren().addAll(formatierung);//Fügt es dem Stackpanel hinzu

        return unten;
    }

    private File getDatei() {
        return cdatei;
    }
}
