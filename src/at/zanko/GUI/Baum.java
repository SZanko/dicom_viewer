package at.zanko.GUI;

import at.zanko.Database.entities.I_instance;
import at.zanko.Database.entities.Patient;
import at.zanko.Database.entities.Series;
import at.zanko.Database.entities.Study;
import javafx.scene.control.TreeItem;

import java.util.List;
//List the Entyties from the Dicom File
class Baum {

    protected TreeItem add(Patient p) {
        TreeItem<String> patient = new TreeItem<>("Patient: " + p.getName());
        patient.setExpanded(true);
        //System.out.println("Patientenname: " + p.getName() + " hinzugefügt");
        List<Study> test_st = p.getStudyList();
        //System.out.println(test_st.toString());
        for (Study st : p.getStudyList()) {
            //System.out.println("In der Studie Schleife");
            TreeItem<String> studie = new TreeItem<>("Studie: " + st.getStudyDescription());
            patient.getChildren().add(studie);
            //System.out.println("Name der Studie: " + p.getStudyList() + " hinzugefügt");
            for (Series se : st.getSeriesList()) {
                TreeItem<String> serie = new TreeItem<>("Serie: " + se.getSeriesDescription());
                studie.getChildren().add(serie);
                //System.out.println("Seriesname: " + p.getSeriesList() + " hinzugefügt");
                for (I_instance in : se.getInstanceList()) {
                    TreeItem<String> instance = new TreeItem<>("Instance" + in.getSopInstanceUID());
                    //System.out.println("Instancename: " + p.getInstanceList() + " hinzugefügt");
                    serie.getChildren().add(instance);
                }
            }
        }


        return patient;
    }


}
