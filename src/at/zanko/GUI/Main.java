package at.zanko.GUI;

import at.zanko.Image_editing.PixelMapper;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {

    static Label Status;

    public static void main(String[] args) {

        launch(args);


        PixelMapper pv = new PixelMapper();


        //pv.Ausgeben(new File("images/test.dcm"));
        //pv.Image_formation(new File("/home/stefan/Dokumente/Schule/3.Klasse/MGIN/Programmieren/MGIN 2.1/Zanko_Dicom_Pixel_Values/Zanko_JAVA_ist _scheisse_viewer_demo/images/test.dcm"),50,1200,800);
        //pv.Image_formation(new File("/home/stefan/Dokumente/Schule/3.Klasse/MGIN/Programmieren/MGIN 2.1/Zanko_Dicom_Pixel_Values/Zanko_JAVA_ist _scheisse_viewer_demo/images/test.dcm"),75,1000,1000);
        //pv.Image_formation(new File("/home/stefan/Dokumente/Schule/3.Klasse/MGIN/Programmieren/MGIN 2.1/Zanko_Dicom_Pixel_Values/Zanko_JAVA_ist _scheisse_viewer_demo/images/test.dcm"),25,500,1500);


    }

    @Override
    public void start(Stage primaryStage) {
        Layout lout=new Layout();
        primaryStage.setTitle("Dicom Viewer");//Setzt den Titel des Fensters

        //primaryStage.setFullScreen(true); //Lässt das Programm im Vollbildmodus starten
        Status = new Label("Status");
        SplitPane aufteilung = lout.Aufteilen();
        Menu_setup mese=new Menu_setup();

        MenuBar menuBar = mese.menuErschaffen();//Holt sich das Menu
        BorderPane layout = new BorderPane();//Legt das Layout des Fensters fest

        layout.setCenter(aufteilung);//Entweder aufteilung oder testpane als Wert
        layout.setBottom(Status);//Setzt den Status unten

        layout.setTop(menuBar);//Setzt das Menu an die Stage oben


        Scene Fenster = new Scene(layout, 800, 600);//Legt die Größe des Fensters fest

        primaryStage.setScene(Fenster);//Was für ein Fenster angzeigt werden soll
        primaryStage.show();//Zum Anzeigen des Fensters
    }
}