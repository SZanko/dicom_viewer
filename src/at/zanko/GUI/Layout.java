package at.zanko.GUI;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

//import javafx.embed.swing.SwingFXUtils;


class Layout  {

    static TreeView<String> baum = new TreeView<>();
    static ImageView bildview;
    private StackPane rechtePane;
    private StackPane linkePane;

    //Soll den Dicom Viewer in 2 Teile teilen einen Linken wo sich Informationen befinden und der Rechte Teil wo das Bild angezeigt wird
    SplitPane Aufteilen() {
        SplitPane splitp = new SplitPane();
        splitp.setOrientation(Orientation.HORIZONTAL);//Setzt die Orientierung des verschiebbaren Strich der Splitpanesplitp.setPadding(new Insets(15, 12, 15, 12));
        Layout layout = new Layout();
        layout.linkePane = layout.linkesPanel();
        layout.rechtePane = new ImagePanel().createImagePanel();
        splitp.getItems().addAll(layout.linkePane, layout.rechtePane);//Zum hinzufügen der Einzelnen Inhalte
        splitp.setDividerPositions(0.31f);//Setzt die Ausgangsposition der Splitpane

        return splitp;
    }


    //Soll die Linke Seite der Splitpane sein
    private StackPane linkesPanel() {
        GridPane gridLinks = new GridPane();
        gridLinks.setPadding(new Insets(2));
        StackPane gesamtPane = new StackPane();
        gesamtPane.setMaxSize(400, 600);
        StackPane unten = new ControllPanel().slider_pane();
        StackPane mitte = new InfoPanel().informationdisp();
        StackPane oben = new PatientBrowser().browserwindow();
        //gesamtPane.setAlignment(oben,Pos.TOP_CENTER);
        //gesamtPane.setAlignment(mitte,Pos.CENTER);
        //gesamtPane.setAlignment(unten,Pos.BOTTOM_CENTER);
        gridLinks.add(oben, 0, 0);
        gridLinks.add(mitte, 0, 1);
        gridLinks.add(unten, 0, 2);


        gesamtPane.getChildren().addAll(gridLinks);
        return gesamtPane;
    }
}
