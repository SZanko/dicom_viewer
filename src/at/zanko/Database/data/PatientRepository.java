package at.zanko.Database.data;

import at.zanko.Database.entities.I_instance;
import at.zanko.Database.entities.Patient;
import at.zanko.Database.entities.Series;
import at.zanko.Database.entities.Study;
import at.zanko.Interface.IObservable;
import at.zanko.Interface.IObserver;
import at.zanko.Interface.IPatientRepository;
import at.zanko.Interface.InterfaceBuilder;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.io.DicomInputStream;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * manages the storage of patients, studies, series and instances in database
 */
public class PatientRepository implements Closeable , IPatientRepository {
    // instance of EntityManager for database activities
    private EntityManager entityManager;

    /**
     * constructor instantiates the EntityManager
     */
    public PatientRepository(){
        entityManager = Persistence
                .createEntityManagerFactory("viewer")
                .createEntityManager();
    }

    /**
     * fetches all the patients from the database
     * @return List of patients
     */
    public List<Patient> getPatientList(){
        return entityManager.createNamedQuery("Patient.findAll", Patient.class)
                .getResultList();
    }

    /**
     * searches for all the files in the folder and sub-folders
     * and passes them to add(...)-method
     *
     * @param folder to be searched
     * @throws IOException in case there is no Folder found
     */
    private void addFolder(File folder) throws IOException {
        if (!folder.isDirectory()){ // argument should be a folder
            throw new IllegalArgumentException(folder.getAbsolutePath() + " is not a folder");
        }
        for (File f : folder.listFiles()){ // iterate through contents of folder
            if (f.isDirectory()){
                addFolder(f); // if item is a sub-folder --> call addFolder() recursively
            }
            else{
                add(f); // pass file to add(...) - method
            }
        }
    }

    /**
     * reads contents of DICOM file and composes the object model
     * patient is stored to database
     * (no commit)
     *
     * @param dicomFile DICOM file to be added to object model
     * @throws IOException
     */
    public void add(File dicomFile) throws IOException {
        // read DicomObject from DICOM file
        DicomInputStream input = new DicomInputStream(dicomFile);
        DicomObject dcm = input.readDicomObject();

        // instantiate objects (patient, study,...) and compose object model
        Patient patient = add(new Patient(dcm));    // add patient to database if necessary
        Study study = patient.add(new Study(dcm));  // add study to patient if necessary
        Series series = study.add(new Series(dcm)); // ...
        series.add(new I_instance(dcm, dicomFile));   // ...

    }

    @Override
    public List<Patient> patients() throws Exception {
        return null;
    }

    /**
     * add a Patient to database if necessary;
     * this method finds an equal patient in the database;
     * if there is one - it's reference is returned
     * otherwise the new Patient is inserted and the reference of this new patient is returned
     *
     * @param p
     * @return Patient
     */
    public Patient add(Patient p){
        Patient patient = entityManager.find(Patient.class, p.getPatientID());
        if (patient == null){   // not in database yet
            entityManager.persist(p);
            patient = p;
        }
        return patient;
    }

    @Override
    public void remove(Patient p) throws Exception {

    }

    /**
     * commit all the changes
     */
    public void commit(){
        entityManager.getTransaction().begin();
        entityManager.getTransaction().commit();
    }


    /**
     * close EntityManager
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        entityManager.close();
    }

    @Override
    public void configure(InterfaceBuilder builder) {

    }

    @Override
    public void registerObserver(IObserver o) {

    }

    @Override
    public void removeObserver(IObserver o) {

    }

    @Override
    public void changed(IObservable o) {

    }
}
