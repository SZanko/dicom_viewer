drop DATABASE if exists MGIN;
create DATABASE if not exists MGIN ;
drop schema if exists Dicom_Viewer;
create schema  if not exists Dicom_Viewer;
use MGIN;

create TABLE if not exists I_instance(
    i_UID Varchar(45) primary key,
    i_number int,
    i_dicomFile Varchar(45),
    i_transferSyntax Varchar(45),
    i_row int,
    i_colums int,
    i_minValue int,
    i_maxValue int
);

create table if not exists S_series(
    se_UID Varchar(45) primary key,
    se_description Varchar(45),
    se_acquisitionTime time
);

create table if not exists P_patient(
    p_ID VARCHAR(45) primary key ,
    p_name Varchar(45),
    p_gebdat date
);

create table if not exists St_study(
    st_UID VARCHAR(45) primary key,
    st_description VARCHAR(45),
    st_date date
);