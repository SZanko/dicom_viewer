package at.zanko.Database.entities;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * contains data of the study and a list of series
 */

@Entity
public class Study {
    @Id
    private String st_UID;
    private String st_description;
    @Temporal(TemporalType.DATE)
    private Date st_date;
    @OneToMany(cascade = CascadeType.ALL)
    //@JoinColumn(name = "studyInstanceUID")
    private List<Series> seriesList = new ArrayList<Series>();

    public Study(){}

    public Study(DicomObject dcm){
        st_UID = dcm.getString(Tag.StudyInstanceUID);
        st_description = dcm.getString(Tag.StudyDescription);
        st_date = dcm.getDate(Tag.StudyDate);
    }

    public Study(String studyID, Date studyDatum, String studyBeschreibung) {
        st_UID=studyID;
        st_date=studyDatum;
        st_description=studyBeschreibung;
    }


    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("Study{studyInstanceUID=").append(st_UID)
                .append(", studyDescription=").append(st_description)
                .append(", studyDate=").append(st_date)
                .append("}");
        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Study study = (Study) o;

        return Objects.equals(st_UID, study.st_UID);
    }

    @Override
    public int hashCode() {
        return st_UID != null ? st_UID.hashCode() : 0;
    }

    public Series add(Series series) {
        int inx = seriesList.indexOf(series);
        if (inx >= 0)
            return seriesList.get(inx);
        seriesList.add(series);
        return series;
    }

    public String getStudyInstanceUID() {
        return st_UID;
    }

    public Date getStudyDate() {
        return st_date;
    }

    public List<Series> getSeriesList() {
        return seriesList;
    }
    public Series find(String SeriesID){
        for (Series s:seriesList) {
            if(s.getSeriesInstanceUID().equals(SeriesID)){
                return s;
            }
        }
        return null;
    }
    public void addSeries(Series series){
        seriesList.add(series);
    }

    public String getStudyDescription() {
        return st_description;
    }
    public void setStudyDescription(String studyDescription) {
        this.st_description = studyDescription;
    }
}
