package at.zanko.Database.entities;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * contains informations of series and a list of images
 */

@Entity
public class Series {
    @Id
    private String se_UID;
    private String se_description;
    @Temporal(TemporalType.TIMESTAMP)
    private Date se_acquisitionTime;

    @OneToMany(cascade = CascadeType.ALL)
   // @JoinColumn(name = "seriesInstanceUid")
    private List<I_instance> instanceList = new ArrayList<I_instance>();


    public Series(){}

    public Series(DicomObject dcm){
        se_UID = dcm.getString(Tag.SeriesInstanceUID);
        se_description = dcm.getString(Tag.SeriesDescription);
        se_acquisitionTime = dcm.getDate(Tag.AcquisitionDateTime);
    }

    public Series(String seriesID, String seriesBeschreibung) {
        se_UID=seriesID;
        se_description=seriesBeschreibung;
    }


    @Override
    public String toString() {
        return new StringBuilder("Series{seriesInstanceUID=").append(se_UID)
                .append(", seriesDescription=").append(se_description)
                .append(", acquisitionTime=").append(se_acquisitionTime)
                .append("}")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Series series = (Series) o;

        return Objects.equals(se_UID, series.se_UID);
    }

    @Override
    public int hashCode() {
        return se_UID != null ? se_UID.hashCode() : 0;
    }

    public I_instance add(I_instance image){
        int inx = instanceList.indexOf(image);
        if (inx >= 0)
            return instanceList.get(inx);
        instanceList.add(image);
        return image;
    }

    public String getSeriesInstanceUID() {
        return se_UID;
    }
    public Date getAcquisitionTime() {
        return se_acquisitionTime;
    }
    public List<I_instance> getInstanceList() {
        return instanceList;
    }
    public void addImage(I_instance img){
        instanceList.add(img);
    }
    public I_instance find(String ImageID){
        for (I_instance i:instanceList) {
            if(i.getSopInstanceUID().equals(ImageID)){
                return i;
            }
        }
        return null;
    }
    public String getSeriesDescription() {
        return se_description;
    }
    public void setSeriesDescription(String seriesDescription) {
        this.se_description = seriesDescription;
    }
}
