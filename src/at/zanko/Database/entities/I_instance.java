package at.zanko.Database.entities;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.File;
import java.util.Objects;

/**
 * data of an image instance
 */

@Entity
public class I_instance {
    @Id
    private String i_UID;
    private int i_number;
    private String i_dicomFile;
    private String i_transferSyntax;
    private int i_rows;
    private int i_columns;
    @Column(name = "i_minValue")
    private int i_minValue;
    @Column(name = "i_maxValue")
    private int i_maxValue;

    public void setI_UID(String i_UID) {
        this.i_UID = i_UID;
    }

    public void setI_number(int i_number) {
        this.i_number = i_number;
    }

    public void setI_dicomFile(String i_dicomFile) {
        this.i_dicomFile = i_dicomFile;
    }

    public void setI_transferSyntax(String i_transferSyntax) {
        this.i_transferSyntax = i_transferSyntax;
    }

    public void setI_rows(int i_rows) {
        this.i_rows = i_rows;
    }

    public void setI_columns(int i_columns) {
        this.i_columns = i_columns;
    }

    public void setI_minValue(int i_minValue) {
        this.i_minValue = i_minValue;
    }

    public void setI_maxValue(int i_maxValue) {
        this.i_maxValue = i_maxValue;
    }

    public I_instance(){}

    public I_instance(DicomObject dcm, File dicomFile) {
        this.i_dicomFile = dicomFile.getAbsolutePath();
        i_UID = dcm.getString(Tag.SOPInstanceUID);
        i_number = dcm.getInt(Tag.InstanceNumber);
        i_transferSyntax = dcm.getString(Tag.TransferSyntaxUID);
        i_rows = dcm.getInt(Tag.Rows);
        i_columns = dcm.getInt(Tag.Columns);
        i_minValue = dcm.getInt(Tag.SmallestImagePixelValue);
        i_maxValue = dcm.getInt(Tag.LargestImagePixelValue);
    }

    public I_instance(String imageId, int imageNumber, int imageRows, Integer imageColums) {
        i_UID=imageId;
        i_number=imageNumber;
        i_rows=imageRows;
        i_columns=imageColums;
    }


    @Override
    public String toString() {
        return "Instance{sopInstanceUID=" + i_UID +
                ", instanceNumber=" + i_number +
                ", dicomFile=" + i_dicomFile +
                ", rows=" + i_rows +
                ", columns=" + i_columns +
                ", minValue=" + i_minValue +
                ", maxValue=" + i_maxValue +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        I_instance instance = (I_instance) o;

        return Objects.equals(i_UID, instance.i_UID);
    }

    @Override
    public int hashCode() {
        return i_UID != null ? i_UID.hashCode() : 0;
    }



    public String getSopInstanceUID() {
        return i_UID;
    }

    public String getDicomFile() {
        return i_dicomFile;
    }

     public int getRows() {
        return i_rows;
    }

    public int getColumns() {
        return i_columns;
    }

    public int getMinValue() {
        return i_minValue;
    }

    public int getMaxValue() {
        return i_maxValue;
    }
}
