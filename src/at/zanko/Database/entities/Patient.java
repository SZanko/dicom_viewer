package at.zanko.Database.entities;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * contains patient data and the patient's studies list
 */
@Entity
// JPQL
@NamedQuery(name="Patient.findAll", query="SELECT p FROM Patient p")
public class Patient {
    @Id
    private String p_ID;
    private String p_name;
    @Temporal(TemporalType.DATE)
    private Date p_gebdat;

    @OneToMany(cascade = CascadeType.ALL)
    //@JoinColumn(name = "p_ID")
    private List<Study> studyList = new ArrayList<>();

    /**
     * @param dcm reads attribute values from dicom object
     */
    public Patient(DicomObject dcm){
        p_ID = dcm.getString(Tag.PatientID);
        p_name = dcm.getString(Tag.PatientName);
        p_gebdat = dcm.getDate(Tag.PatientBirthDate);
    }

    public Patient(String patId, String patName, Date patBirth) {
        p_ID=patId;
        p_name=patName;
        p_gebdat=patBirth;
    }

    public Patient() {
    }
/*
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("Patient{patientID=").append(patientID)
                .append(", name=").append(name)
                .append(", birthDate=").append(birthDate)
                .append("}");
        return str.toString();
    }*/

    /**
     * defines equal patients (equal if patientIDs are equal)
     * @param o the compared Object
     * @return the Difference between the Objekts
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;

        Patient patient = (Patient) o;

        return Objects.equals(p_ID, patient.p_ID);
    }

    @Override
    public int hashCode() {
        return p_ID != null ? p_ID.hashCode() : 0;
    }

    /**
     * if equal study (like the one passed in parameter)
     * is contained in study list: returns reference to found study
     * if not: adds study to study list and returns reference to this new
     * inserted study
     *
     * @param study to be inserted if not in list
     * @return reference to new inserted study or to equal study already in list
     */
    public Study add(Study study){
        int inx = studyList.indexOf(study);
        if (inx >= 0)
            return studyList.get(inx);
        studyList.add(study);
        return study;
    }

    public String getPatientID() {
        return p_ID;
    }

    public String getName() {
        return p_name;
    }

    public Date getBirthDate() {
        return p_gebdat;
    }

    public List<Study> getStudyList() {
        return studyList;
    }

    private ArrayList<Series> getSeriesList(){ // To get all series specificly for a patient
        ArrayList<Series> ret = new ArrayList<>();
        for(Study s : studyList){
            ret = (ArrayList<Series>) s.getSeriesList();
        }
        return ret;
    }

    //Zum Bekommen von allen Bildern des Patientens
    public ArrayList<I_instance> getInstanceList(){
        ArrayList<I_instance> ret = new ArrayList<>();
        ArrayList<Series> serL = getSeriesList();//Holt sich die vorigen Studien des Patienten
        for(Series s : serL){//Geht Series so lange durch bis alle Bilder in
            ret = (ArrayList<I_instance>) s.getInstanceList();
        }
        return ret;
    }
    public void addStudy(Study s) {
        if (!studyList.contains(s)) studyList.add(s);
    }
    public Study find(String StudyID){
        for (Study s:studyList) {
            if(s.getStudyInstanceUID().equals(StudyID)){
                return s;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "PatientID='" + p_ID + '\'' +
                ", Name='" + p_name + '\'' +
                ", birthDate=" + p_name +
                '}';
    }
}
